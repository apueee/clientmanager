<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Api extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index(){
       // $this->load->view('theme/login.php');
    }

    function login() {
        $this->load->model('usersmodel','um');
        $isSaleman = false;

        $response = $this->um->login($this->input->post('username') ,$this->input->post('password'));
        $data = array();
        if($response) {
            $data['success'] = true;
            $data['message'] = 'Login Success';
            $userinfo = $this->um->getUserDetails($response[0]['id']);
            $userroles = $this->um->getUserRole($response[0]['id']);
            foreach($userroles as $role) {
                if($role['role_id'] ==2) {
                    $isSaleman = true;
                    break;
                }
            }

        }
        if($isSaleman) {
            $data['userinfo'] = $userinfo[0];
            header('Content-type: application/json');
            die(json_encode($data));
        }
        else {
            $data['success'] = false;
            $data['message'] = 'Login failed';
            header('Content-type: application/json');
            die(json_encode($data));
        }

    }
    function checkin () {
        $this->load->model('clientsmodel','cm');
        $this->client_id     =  $this->input->post('clientid');
        $this->salesman_id   =  $this->input->post('salesmanid');
        $this->location_id   =  $this->input->post('locationid');
        $this->message       =  $this->input->post('message');
        $this->is_approved   =  '0';
        $this->status = "checkin";

        if($this->db->insert('scheduled_visits', $this)) {
            $data['success'] = true;
            $data['message'] = 'checked in successfuly';
            header('Content-type: application/json');
            die(json_encode($data));
        }
        $data['success'] = false;
        $data['message'] = 'Login failed';
        header('Content-type: application/json');
        die(json_encode($data));
    }
    function nearbyclients() {
       // print_r($this->input->post());die;
        $this->load->model('clientsmodel','cm');
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');
        $response = $this->cm->getNearBy($lat, $lng);
        //die(print_r($response));
        if($response) {
            $data['success'] = true;
            $data['clientList'] = $response;
            header('Content-type: application/json');
            die(json_encode($data));
        } else {
            $data['success'] = false;
            $data['message'] = 'failed';
            header('Content-type: application/json');
            die(json_encode($data));
        }
    }
    function myshcedules() {
        $userId = $this->input->post('userid');
        if($userId) {
            $this->load->model("schedulemodel", "s");
            $response = $this->s->getBySaleman($userId, 'pending');
        }

        if($response) {
            $data['success'] = true;
            $data['clientList'] = $response;
            header('Content-type: application/json');
            die(json_encode($data));
        } else {
            $data['success'] = false;
            $data['message'] = 'failed';
            header('Content-type: application/json');
            die(json_encode($data));
        }
    }
    function checkedin() {
        $id = $this->input->post('id');
        if($id) {
            $this->load->model("schedulemodel", "s");
            $this->status     =  'checkin';

            if($this->db->update('scheduled_visits', $this, array('id' => $id))) {
                $data['success'] = true;
                header('Content-type: application/json');
                die(json_encode($data));
            }
        }
        $data['success'] = false;
        $data['message'] = 'failed';
        header('Content-type: application/json');
        die(json_encode($data));
    }
    function getmarketniches() {
        $this->load->model('marketnichemodel', 'mkt_niche');
        $response = $this->mkt_niche->getAll();

        if($response) {
            $data['success'] = true;
            $data['marketList'] = $response;
            header('Content-type: application/json');
            die(json_encode($data));
        } else {
            $data['success'] = false;
            $data['message'] = 'failed';
            header('Content-type: application/json');
            die(json_encode($data));
        }
    }
    function getuserinfo() {
//        $this->load->model("usersmodel", "u");
//        $userid = $this->input->post('userid');
//        $response = $this->u->getUser($userid);
//
//        if($response) {
//            $data['success'] = true;
//            $data['us'] = $response;
//            header('Content-type: application/json');
//            die(json_encode($data));
//        } else {
//            $data['success'] = false;
//            $data['message'] = 'failed';
//            header('Content-type: application/json');
//            die(json_encode($data));
//        }
    }
    function  updateprofile() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sur_name', 'Sur Name', 'trim|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        if(count($_POST) > 0){
            if ($this->form_validation->run() ) {
                $this->name              =  $this->input->post('name');
                $this->sur_name          =  $this->input->post('sur_name');
                $this->email             =  $this->input->post('email');
                //$this->password          =  md5($this->input->post('password'));

                $userid = $this->input->post('userid');

                if($userid && $this->db->update('users', $this, array('id' => $userid))) {
                    $data['success'] = true;
                    $this->load->model('usersmodel','um');
                    $userinfo = $this->um->getUserDetails($userid);
                    $data['userinfo'] = $userinfo[0];
                    header('Content-type: application/json');
                    die(json_encode($data));
                } else  {
                    $data['success'] = false;
                    $data['message'] = 'failed';
                    header('Content-type: application/json');
                    die(json_encode($data));
                }
            }
        }
    }
    function addclient() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Client Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('address', 'Address', 'trim|required|xss_clean');
        $this->form_validation->set_rules('lat', 'Lat', 'trim|required|xss_clean');
        $this->form_validation->set_rules('lon', 'Lon', 'trim|required|xss_clean');
        $this->form_validation->set_rules('location_name', 'Location Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sap_code', 'Sap Code', 'trim|required||xss_clean');
        $this->form_validation->set_rules('market', 'Market Niche', 'trim|required|xss_clean');
        $this->form_validation->set_rules('description', 'Description', 'trim|xss_clean');

        if(count($_POST) > 0){
            //print_r($_POST);die;
            if ($this->form_validation->run()) {
                $this->name               = $this->input->post('name');
                $this->sap_code           =  $this->input->post('sap_code');
                $this->market_niche_id    =  $this->input->post('market');
                $this->description        =  $this->input->post('description');

                $this->db->insert('clients', $this);
                $clientId = $this->db->insert_id();
                $locData = array(
                    'address' => $this->input->post('address'),
                    'name' => $this->input->post('location_name'),
                    'lat' => $this->input->post('lat'),
                    'lon' => $this->input->post('lon')
                );
                if($this->db->insert("locations", $locData)) {
                    $locId = $this->db->insert_id();
                    $this->db->insert("clients_locations", array('client_id' => $clientId, 'location_id' => $locId));

                }
                $data['success'] = true;
                header('Content-type: application/json');
                die(json_encode($data));

            } else {
                $data['success'] = false;
                $data['message'] = 'failed';
                header('Content-type: application/json');
                die(json_encode($data));
            }
        }
    }
}
?>
