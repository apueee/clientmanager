<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Schedule extends CI_Controller 
{


    function __construct()
    {
        parent::__construct();

        $this->load->model("schedulemodel", "s");
        //$this->session->start();
        if($this->session->userdata("loggedin")!='1'){
            redirectToLogin();
            die();
        }
        parse_str($_SERVER['QUERY_STRING'], $_GET);
    }
    function index()
    {
        $default = array('page', 'filter', 'category', 'location', 'client', 'salesman', 'from', 'to');

        $params = $this->uri->uri_to_assoc(2, $default); //print_r($params);die;
        $data['pagetitle'] = "Home > Check a visit";
        if($this->session->userdata("loggedin")!='1'){
            $this->load->view('theme/login.php');
        } else {
            $page =  ($params['page'] != '') ? $params['page'] : 1;
            $per_page = 2;
            $offset = ($page -1) * $per_page;
            $this->load->library('pagination');
            $this->load->model('clientsmodel', 'cm');
            $this->load->model('marketnichemodel', 'mktm');
            $this->load->model("locationsmodel", "l");

            $filterByCategory =  ($params['category'] != '') ? $params['category'] : NULL;
            $filterByLocation =  ($params['location'] != '') ? $params['location'] : NULL;
            $filterByClient =  ($params['client'] != '') ? $params['client'] : NULL;
            $filterBySalesman =  ($params['salesman'] != '') ? $params['salesman'] : NULL;
            $filterByfrom =  ($params['from'] != '') ? $params['from'] : NULL;
            $filterByTo =  ($params['to'] != '') ? $params['to'] : NULL;
            $filterByStatus = 'pending';
            $urisegments = array();
            if($params['category'] != '') {
                $urisegments['category'] = $filterByCategory;
            }
            if($params['location'] != '') {
                $urisegments['location'] = $filterByLocation;
            }
            if($params['client'] != '') {
                $urisegments['client'] = $filterByClient;
            }
            if($params['salesman'] != '') {
                $urisegments['salesman'] = $filterBySalesman;
            }
            if($params['from'] != '') {
                $urisegments['from'] = $filterByfrom;
            }
            if($params['to'] != '') {
                $urisegments['to'] = $filterByTo;
            }


            $config['base_url'] =  $this->config->item("base_url") . 'schedule/' . $this->uri->assoc_to_uri($urisegments) . '/page/';
            $config['total_rows'] = $this->s->getTotalCount($filterByCategory, $filterByLocation, $filterByClient, $filterBySalesman,$filterByfrom,$filterByTo, $filterByStatus);
            $config['per_page'] = $per_page;
            $config['use_page_numbers'] = TRUE;
            $data['total_count'] = $config['total_rows'];
            $config['cur_tag_open'] = '<a href="#" class="active">';
            $config['cur_tag_close'] = '</a>';
            $this->pagination->initialize($config);
            $response = $this->s->getAll($per_page, $offset, $filterByCategory, $filterByLocation, $filterByClient,$filterBySalesman,$filterByfrom,$filterByTo, $filterByStatus);
            if($response) {
                $data['scheduleList'] = $response;//die;
            }

            $data['categoryList'] =  $this->mktm->getAll();
            
            $locationList = ($filterByClient) ? $this->l->getLocationsByClient($filterByClient) : $this->l->getAll();
            $data['locationList'] = $locationList;
            $data['clientList'] = $this->cm->getAll(NULL, 0 , $filterByCategory);
            $this->load->model('usersmodel', 'u');
            $data['salesManList'] = $this->u->getByRole('Sales Man');

            $data['filterBycategory'] = $filterByCategory;
            $data['filterBylocation'] = $filterByLocation;
            $data['filterByClient'] = $filterByClient;
            $data['filterBySalesman'] = $filterBySalesman;
            $data['filterByFrom'] = $filterByfrom;
            $data['filterByTo'] = $filterByTo;
            $urisegments['status'] = "pending";
            $data['exporturl'] = $this->config->item("base_url") . 'schedule/export/' . $this->uri->assoc_to_uri($urisegments);
            $this->load->view('theme/schedulelist',$data);
        }
    }
    function checkedin()
    {
        $default = array('page', 'filter', 'category', 'location', 'client', 'salesman', 'from', 'to');

        $params = $this->uri->uri_to_assoc(3, $default); //print_r($params);die;
        $data['pagetitle'] = "Home > Check a visit";
        if($this->session->userdata("loggedin")!='1'){
            $this->load->view('theme/login.php');
        } else {
            $page =  ($params['page'] != '') ? $params['page'] : 1;//die('>>>'.$page);
            $per_page = 5;
            $offset = ($page -1) * $per_page;
            $this->load->library('pagination');
            $this->load->model('clientsmodel', 'cm');
            $this->load->model('marketnichemodel', 'mktm');
            $this->load->model("locationsmodel", "l");

            $filterByCategory =  ($params['category'] != '') ? $params['category'] : NULL;
            $filterByLocation =  ($params['location'] != '') ? $params['location'] : NULL;
            $filterByClient =  ($params['client'] != '') ? $params['client'] : NULL;
            $filterBySalesman =  ($params['salesman'] != '') ? $params['salesman'] : NULL;
            $filterByfrom =  ($params['from'] != '') ? $params['from'] : NULL;
            $filterByTo =  ($params['to'] != '') ? $params['to'] : NULL;
            $urisegments = array();

            if($params['category'] != '') {
                $urisegments['category'] = $filterByCategory;
            }
            if($params['location'] != '') {
                $urisegments['location'] = $filterByLocation;
            }
            if($params['client'] != '') {
                $urisegments['client'] = $filterByClient;
            }
            if($params['salesman'] != '') {
                $urisegments['salesman'] = $filterBySalesman;
            }
            if($params['from'] != '') {
                $urisegments['from'] = $filterByfrom;
            }
            if($params['to'] != '') {
                $urisegments['to'] = $filterByTo;
            }
            $filterByStatus = 'checkin';
//            if($page > 1)
//                $urisegments['page'] = $page;
            $config['base_url'] =  $this->config->item("base_url") . 'schedule/checkedin/' . $this->uri->assoc_to_uri($urisegments) . '/page/';
            $config['total_rows'] = $this->s->getTotalCount($filterByCategory, $filterByLocation, $filterByClient, $filterBySalesman,$filterByfrom,$filterByTo, $filterByStatus);
            $config['per_page'] = $per_page;
            $config['use_page_numbers'] = TRUE;
            $data['total_count'] = $config['total_rows'];
            $config['cur_tag_open'] = '<a href="#" class="active">';
            $config['cur_tag_close'] = '</a>';
            $this->pagination->initialize($config);
            $response = $this->s->getAll($per_page, $offset, $filterByCategory, $filterByLocation, $filterByClient,$filterBySalesman,$filterByfrom,$filterByTo, $filterByStatus);
            if($response) {
                $data['scheduleList'] = $response;//die;
            }

            $data['categoryList'] =  $this->mktm->getAll();

            $locationList = ($filterByClient) ? $this->l->getLocationsByClient($filterByClient) : $this->l->getAll();
            $data['locationList'] = $locationList;
            $data['clientList'] = $this->cm->getAll(NULL, 0 , $filterByCategory);
            $this->load->model('usersmodel', 'u');
            $data['salesManList'] = $this->u->getByRole('Sales Man');

            $data['filterBycategory'] = $filterByCategory;
            $data['filterBylocation'] = $filterByLocation;
            $data['filterByClient'] = $filterByClient;
            $data['filterBySalesman'] = $filterBySalesman;
            $data['filterByFrom'] = $filterByfrom;
            $data['filterByTo'] = $filterByTo;
            $urisegments['status'] = "checkin";
            $data['exporturl'] = $this->config->item("base_url") . 'schedule/export/' . $this->uri->assoc_to_uri($urisegments);
            $this->load->view('theme/checkedinlist',$data);
        }
    }
    function add()
    {
        $data['pagetitle'] = 'Home > Scheduled Visit > Add';

        if($this->session->userdata("loggedin")!='1'){
            $this->load->view('theme/login.php');
        } else {
            $this->load->library('form_validation');
            //$this->load->helper(array('form', 'url'));

            $this->form_validation->set_rules('client', 'Client Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('salesman', 'Salesman', 'trim|required|xss_clean');
            $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
          
            if(count($_POST) > 0){

                if ($this->form_validation->run() == FALSE)
                {

                } else {
                    $this->client_id     =  $this->input->post('client');
                    $this->salesman_id   =  $this->input->post('salesman');
                    $this->location_id   =  $this->input->post('location');
                    $this->is_approved   =  '1';
                   
                    $this->db->insert('scheduled_visits', $this);
                    header("Location:{$this->config->item("base_url")}schedule");
                }

            }
            $data['schedule_data'] = null;
            $this->load->model('clientsmodel', 'cm');
            $clientList = $this->cm->getAll();
            $data['clientList'] = $clientList;
            $this->load->model('usersmodel', 'u');
            $data['salesManList'] = $this->u->getByRole('Sales Man');
            $this->load->model('locationsmodel', 'l');
            $data['locationList'] = $this->l->getAll(); //print_r($data['locationList']);die;
            $this->load->view('theme/scheduleadd',$data);
        }
    }
    function edit($id)
    {
        $data['pagetitle'] = 'Home > Scheduled Visit > Edit';

        if($this->session->userdata("loggedin")!='1'){
            $this->load->view('theme/login.php');
        } else {
            $this->load->library('form_validation');
            //$this->load->helper(array('form', 'url'));

            $this->form_validation->set_rules('client', 'Client Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('salesman', 'Salesman', 'trim|required|xss_clean');
            $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
          
            if(count($_POST) > 0){

                if ($this->form_validation->run() )
                {
                    $this->client_id     =  $this->input->post('client');
                    $this->salesman_id   =  $this->input->post('salesman');
                    $this->location_id   =  $this->input->post('location');
                    
                    $this->db->update('scheduled_visits', $this, array('id' => $id));
                    header("Location:{$this->config->item("base_url")}schedule");
                } 
            }
            
            $data['schedule_data'] = $this->s->getById($id);
            $this->load->model('clientsmodel', 'cm');
            $clientList = $this->cm->getAll();
            $data['clientList'] = $clientList;
            $this->load->model('usersmodel', 'u');
            $data['salesManList'] = $this->u->getByRole('Sales man');
            $this->load->model('locationsmodel', 'l');
            $data['locationList'] = $this->l->getAll();
            $this->load->view('theme/scheduleadd',$data);
        }
    }
    function delete($id)
    {
        $data['pagetitle'] = 'Delete Schedule';
        if($this->session->userdata("loggedin")!='1'){
            $this->load->view('theme/login.php');
        } else {
            $this->s->delete($id);
            header("Location:{$this->config->item("base_url")}schedule");
        }
    }
    function export()
    {
        $default = array('page', 'filter', 'category', 'location', 'client', 'from', 'to', 'status');

        $params = $this->uri->uri_to_assoc(3, $default);
        //print_r($params);die;
        $filterByCategory =  (isset ($params['category']) && $params['category'] != '') ? $params['category'] : NULL;
        $filterByLocation =  (isset ($params['location']) && $params['location'] != '') ? $params['location'] : NULL;
        $filterByClient =  (isset ($params['client']) && $params['client'] != '') ? $params['client'] : NULL;
        $filterBySalesman =  (isset ($params['salesman']) && $params['salesman'] != '') ? $params['salesman'] : NULL;
        $filterByfrom =  (isset ($params['from']) && $params['from'] != '') ? $params['from'] : NULL;
        $filterByTo =  (isset ($params['to']) && $params['to'] != '') ? $params['to'] : NULL;
        $filterByStatus =  (isset ($params['status']) && $params['status'] != '') ? $params['status'] : NULL;
        $reports = $this->s->getAll(NULL, 0, $filterByCategory, $filterByLocation, $filterByClient, $filterBySalesman, $filterByfrom, $filterByTo, $filterByStatus);
        $this->exportAsCsv('Schedule List', $reports);
        return true;
    }
    function isok($id) {
        if($id) {
            $this->load->model("schedulemodel", "s");
            $this->is_approved     =  '1';

            if($this->db->update('scheduled_visits', $this, array('id' => $id))) {
                $data['success'] = true;

            }
        }
        header("Location:{$this->config->item("base_url")}schedule/checkedin");
    }
    function exportAsCsv($title, $getReports)
    {
        $csvFile = APPPATH.'../public/static/new.csv';

        $fp = fopen($csvFile, 'w');
        $arrayToWrite = array();
        $i=0;
        
        foreach ($getReports as $fields) {
            if($i == 0){
                $getValue['client_name'] = 'Client Name';
                $getValue['salesman_name'] = 'Salesman Name';
                $getValue['address'] = 'Location';
                $arrayToWrite[$i++] = $getValue;
            }
            $getValue['client_name'] = $fields['client_name'];
          
            $getValue['salesman_name'] = $fields['salesman_name'];
            $getValue['address'] = $fields['address'];
            $arrayToWrite[$i] = $getValue;
            
            $i++;
        }
        
        foreach ($arrayToWrite as $write):
            fputcsv($fp, $write);
        endforeach;

        fclose($fp);
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="'.$title.'.csv"');
        readfile($csvFile);
        exit;

    }
}