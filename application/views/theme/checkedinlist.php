<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once 'header.php';
include_once 'navigation.php';
?>

<div class="block small right">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2><?php echo $pagetitle;?></h2>

        <ul>
            <li><a href="<?php echo $baseurl?>schedule/add">+ Add New</a></li>
        </ul>
    </div>		<!-- .block_head ends -->

    <div class="block_content">
        <div class="filter">
            <select name="salesman" id="salesman"  style="width: 180px;">
                <option value="">Filter by Salesman</option>
                <?php if(isset($salesManList) && count($salesManList) > 0) :
                foreach($salesManList as $val) :
                    ?>
                    <option value="<?php echo $val['id'];?>" <?php echo ($val['id'] == $filterBySalesman) ? 'selected': '';?>><?php echo $val['name'];?></option>
                    <?php endforeach; endif;?>
            </select>&nbsp;&nbsp;
            <select name="category" id="category" style="width: 180px;">
                <option value="">Filter by Category</option>
                <?php foreach ($categoryList as $category):?>
                <option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $filterBycategory) ? 'selected': '';?>><?php echo $category['title'];?></option>
                <?php endforeach;?>
            </select>&nbsp;&nbsp;

            <select name="client" id="client"  style="width: 180px;">
                <option value="">Filter by Client</option>
                <?php if(isset($clientList) && count($clientList) > 0) :
                foreach($clientList as $val) :
                    ?>
                    <option value="<?php echo $val['id'];?>" <?php echo ($val['id'] == $filterByClient) ? 'selected': '';?>><?php echo $val['name'];?></option>
                    <?php endforeach; endif;?>
            </select>&nbsp;&nbsp;
            <select name="location" id="location" style="width: 180px;">
                <option value="">Filter by Location</option>
                <?php foreach ($locationList as $location): if($location['name']!= '') :?>
                <option value="<?php echo $location['id'];?>" <?php echo ($location['id'] == $filterBylocation) ? 'selected': '';?>><?php echo $location['name'];?></option>
                <?php endif;endforeach;?>
            </select>

        </div> <br /><br />

        <p style="float:left">
            <label>From:</label>
            <input id="startdate"  type="text" value="<?php echo isset ($filterByFrom) ? $filterByFrom: '';?>" style="width: 20%"  />
            &nbsp;&nbsp;
            <label>To:</label>
            <input id="enddate" type="text"  value="<?php echo isset ($filterByTo) ? $filterByTo: '';?>" style="width: 20%" />

        </p>
            <table width="100%" cellspacing="0" cellpadding="0">

                <tbody><tr>
                    <th width="180">Client Name</th>
                    <th width="180">Salesman Name</th>
                    <th width="100">Location</th>
                    <th width="100">Message</th>
                    <th width="80">Status</th>
                    <td width="40">&nbsp;</td>
                </tr>
                <?php if(isset($scheduleList) && count($scheduleList) > 0) :
                    foreach($scheduleList as $val) :
                ?>
                <tr style="background-color: rgb(251, 251, 251);">
                    <td><a href="#"><?php echo $val['client_name'];?></a></td>
                    <td><?php echo $val['salesman_name'];?></td>
                    <td><?php echo $val['location_name'];?></td>
                    <td><?php
                        if(strlen($val['message']) > 15) {
                            $msg = substr($val['message'], 0, 15);
                            echo $msg . '<span style="color:blue;cursor:pointer" onclick="javascript:alert(\'' . $val['message'] . '\')">...more</span>';
                        } else {
                            echo $val['message'];
                        }
                        ?></td>

                    <td><?php echo ($val['is_approved'] == '1') ? 'Approved' : 'Not Approved';?></td>
                    <td class="delete"> <?php if($val['is_approved'] != '1') :?><a  style="color:#008EE8;" onclick="return confirm('Are you sure to ok this?')" href="<?php echo $baseurl.'schedule/isok/'.$val['id'];?>">checkin</a><?php endif;?></td>
                </tr>
                <?php endforeach; else :?>
                <tr style="background-color: rgb(251, 251, 251);">
                    <td colspan="4" > No client schedule added yet</td>
                </tr>
                    <? endif;?>
                </tbody>
            </table>

            <div class="paggination right">
                <?php echo $this->pagination->create_links();?> Total : <?php echo $total_count;?>

            </div>		<!-- .paggination ends -->
        
        <p class="pull-right"><a href="<?php echo $exporturl;?>" >Export As CSV</a></p>
    </div>
    <!-- .block_content ends -->

    <div class="bendl"></div>
    <div class="bendr"></div>

</div>
<?
//include_once 'semifooter.php';
//include_once 'footer.php';

?>
<style>
    p.pull-right
    {
        text-align: right;
        clear: both;
        padding-top: 20px;
    }
</style>

<script type="text/javascript">

    //$filterBySalesman
    $('select[name="salesman"]').change(function() {
        updateUrl();
    });
    $('select[name="category"]').change(function() {
        updateUrl();
    });
    $('select[name="location"]').change(function() {
        updateUrl();
    });
    $('select[name="client"]').change(function() {
        updateUrl();
    });
    $('#startdate').datepicker({dateFormat: "yy-mm-dd"});
    $('#enddate').datepicker({
        dateFormat: "yy-mm-dd" ,
        onSelect: function(dateText, inst){
            updateUrl();
        }


    });
    function updateUrl() {
        var categoryId = $("#category").val();
        var locationId = $("#location").val();
        var clientId = $("#client").val();
        var salesmanId = $("#salesman").val();
        var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();

        var url = '<?php echo $baseurl.'schedule/'; ?>';
        if(categoryId) {
            url += 'category/' + categoryId + '/';
        }
        if(locationId) {
            url += 'location/' + locationId + '/';
        }
        if(clientId) {
            url += 'client/' + clientId + '/';
        }
        if(salesmanId) {
            url += 'salesman/' + salesmanId + '/';
        }
        if(startdate) {
            url += 'from/' + startdate + '/';
        }
        if(enddate) {
            url += 'to/' + enddate + '/';
        }
        url += 'status/isok';
        window.location = url;
    }
</script>