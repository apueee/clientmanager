<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once 'header.php';
include_once 'navigation.php';
?>
<div class="block">

  	<!-- .block_head ends -->

    <div class="block withsidebar">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2>All Client List</h2>
    </div>		<!-- .block_head ends -->



    <div class="block_content">

    <div class="sidebar">
        <ul class="sidemenu">
            <li class=""><a href="<?=$baseurl?>admin/">Home</a></li>
            <li class="active"><a href="<?=$baseurl?>admin/client">Manage Clients</a></li>
            <li class=""><a href="#sb3">Manage Market Niches</a></li>
            <li class=""><a href="#sb1">Schedule A visit</a></li>
            <li><a href="#sb2">Manage User</a></li>
            <li><a href="#sb3">Manage Roles</a></li>
        </ul>

     </div>		<!-- .sidebar ends -->
     <div class="block_content">
         <form accept="" method="post">
         <table cellpadding="0" cellspacing="0" width="100%">

                <tr>
                    <th>Client Name</th>
                    <td>Sap Code</td>
                    <td>Market Niche</td>
                    <td>&nbsp;</td>
                </tr>

                <?php
                foreach($allClient as $clientdata)
                    {
                    ?>
                    <tr id='ut<?=$clientdata->id?>'>
                        <td><?=$clientdata->name?></td>
                        <td><?=$clientdata->sap_code?></td>
                        <td><?=$clientdata->market_niche_id?></td>
                        <td class="delete">
                           <a id='ue<?=$clientdata->id;?>' style="cursor:pointer" onclick="">Details</a> | <a id='ue<?=$clientdata->id;?>' style="cursor:pointer" onclick="editclient('<?=$clientdata->id;?>')">Edit</a> | <a style="cursor:pointer" onclick="delClient(<?=$clientdata->id;?>,'ut');">Delete</a>
                        </td>
                    </tr>
                    <?php
                    }
                ?>


            </table>
             </form>
     </div>
    </div>
     <div class="bendl"></div>
    <div class="bendr"></div>
    </div>
    </div>
    