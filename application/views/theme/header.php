<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$ci = get_instance();
$baseurl = $ci->config->item("base_url");

$message = $this->session->flashdata('message');
$messagetype = $this->session->flashdata('messagetype');
$userrole = $this->session->userdata("role");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Client Manager</title>
        <link href="<?=$baseurl?>static/css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?=$baseurl?>static/css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
        <link href="<?=$baseurl?>static/css/facebox.css" rel="stylesheet" type="text/css" />
        <link href="<?=$baseurl?>static/css/visualize.css" rel="stylesheet" type="text/css" />
        <link href="<?=$baseurl?>static/css/date_input.css" rel="stylesheet" type="text/css" />
        <link href="<?=$baseurl?>static/css/jquery.jgrowl.css" rel="stylesheet" type="text/css" />
        <link href="<?=$baseurl?>static/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
        <link href="<?=$baseurl?>static/css/jquery.ui.all.css" rel="stylesheet" type="text/css" />
        <link href="<?=$baseurl?>static/css/jquery-ui-1.7.3.custom.css" rel="stylesheet" type="text/css" />


	<!--[if lt IE 8]><style type="text/css" media="all">@import url("css/ie.css");</style><![endif]-->

    <script type="text/javascript" src="<?=$baseurl?>static/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/jquery.img.preload.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/jquery.filestyle.mini.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/jquery.wysiwyg.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/jquery.date_input.pack.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/facebox.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/excanvas.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/jquery.visualize.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/jquery.select_skin.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/jquery.pngfix.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/jquery.scrollTo-1.4.2-min.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/custom.js"></script>
    <script type="text/javascript" src="<?=$baseurl?>static/js/jquery.jgrowl.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<!--    <script type="text/javascript" src="--><?//=$baseurl?><!--static/js/jquery.validationEngine.js"></script>-->
<!--    <script type="text/javascript" src="--><?//=$baseurl?><!--static/js/jquery.validationEngine-en.js"></script>-->
    <script src="<?php echo $baseurl?>static/js/jquery-ui-1.8.7.min.js"></script>
    <script src="<?php echo $baseurl?>static/js/jquery.ui.addresspicker.js"></script>
        
        <script type="text/javascript">
             var baseurl = "<?=$baseurl;?>";
        </script>
</head>

    <body>

	<div id="hld">

		<div class="wrapper">
