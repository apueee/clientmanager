<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once 'header.php';

?>
<div class="block small center login"  style="margin-bottom:150px">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2><?=$ci->lang->line('login_title');
?></h2>
        <!--
         <ul>
             <li><a href="#">back to the site</a></li>
         </ul>-->
    </div>		<!-- .block_head ends -->




    <div class="block_content">
        

        <?php
        if($message!="")
            echo "<div class='message {$messagetype}'><p>{$message}</p></div>";
        ?>

        <form action="<?=$baseurl?>admin/login" method="post">
            <p>
                <label>Username:</label><br />
                <input type="text" name="username" class="text" />
            </p>

            <p>
                <label>Password:</label><br />
                <input type="password" class="text" name="password" />
            </p>

            <p>
                <input type="submit" class="submit" value="Login" /> &nbsp;
                <input type="checkbox" class="checkbox" checked="checked" id="rememberme" /> <label for="rememberme">Remember me</label>
            </p>
        </form>

    </div>		<!-- .block_content ends -->

    <div class="bendl"></div>
    <div class="bendr"></div>

</div>		<!-- .login ends -->

<?php
include_once 'footer.php';
?>