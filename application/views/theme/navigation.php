<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

?>


<div id="header">
    <div class="hdrl"></div>
    <div class="hdrr"></div>

    <h1><a href="#"><?=$ci->lang->line('admin_text');?></a></h1>

    <ul id="nav">
        <li class="" >
            <a href="<?=$baseurl;?>admin/index">DASHBOARD</a>
        </li>

    </ul>

    <p class="user"><a href="<?=$baseurl?>admin/logout">Logout</a></p>
</div>		<!-- #header ends -->
<div class="block tiny left">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2>Menu</h2>
    </div>		<!-- .block_head ends -->



    <div class="block_content">

        <ul class="sidemenu">
            <li class=""><a href="<?php echo $baseurl?>admin">Home</a></li>
            <li class=""><a href="<?php echo $baseurl?>clients">Manage Clients</a></li>
            <li class=""><a href="<?php echo $baseurl?>marketniche">Manage Market Niches</a></li>
            <li class=""><a href="<?php echo $baseurl?>schedule">Check pending visit</a></li>
            <li class=""><a href="<?php echo $baseurl?>schedule/checkedin">Check Salesman visits</a></li>
            <li><a href="<?php echo $baseurl?>users">Manage User</a></li>
            <li><a href="<?php echo $baseurl?>roles">Manage Roles</a></li>
        </ul>

    </div>		<!-- .block_content ends -->

    <div class="bendl"></div>
    <div class="bendr"></div>

</div>