<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once 'header.php';
include_once 'navigation.php';
$edit = (count($schedule_data) > 0) ? true : false;
$edit = (count($_POST) > 0 ) ? false : true;
?>

<div class="block small right">

    <div class="block_head">
        <div class="bheadl"></div>
        <div class="bheadr"></div>

        <h2><?php echo $pagetitle; ?></h2>

        <ul>
            <li><a href="<?php echo $baseurl ?>schedule">Back</a></li>
        </ul>
    </div>		<!-- .block_head ends -->
    <div class="block_content">
        <form method="post" action="#">
            <p>
                <table>
                    <tr>
                        <th>Salesman</th>
                        <th>Client Name</th>
                        <th>Location</th>
                    </tr>
                    <tr>
                        <td>
                         <select class="" name="salesman" id="salesmanList" size="4" >
                             <?php foreach ($salesManList as $salesman):?>
                             <option value="<?php echo $salesman['id'];?>" <?php echo set_value('salesman', ($edit && $schedule_data[0]['salesman_id'] == $salesman['id']) ? 'selected' : '');?>><?php echo $salesman['name'];?></option>
                             <?php endforeach;?>
                         </select>
                        <?php echo form_error('salesman', '<span class="note error">', '</span>'); ?>
                        </td>
                        <td>
                         <select class="" name="client" id="clientList" size="4" >
                             <?php foreach ($clientList as $client):?>
                             <option value="<?php echo $client['id'];?>" <?php echo set_value('client', ($edit && $schedule_data[0]['client_id'] == $client['id']) ? 'selected' : '');?>><?php echo $client['name'];?></option>
                             <?php endforeach;?>
                         </select>
                            <?php echo form_error('client', '<span class="note error">', '</span>'); ?>
                        </td>
                        <td>
                         <select class="" name="location" id="locationList" size="4"  >
                            <?php //print_r($locationList);die;
                            foreach($locationList as $location){
//                                if($edit && $schedule_data[0]['location_id'] == $location['id'])
//                                {
//                                    echo '<option value="'.$location['id'].'" selected >'.$location['name'].'</option>';
//                                } else {
//
//                                }
                                echo '<option value="'.$location['id'].'"  >'.$location['name'].'</option>';
                            }
                            ?>
                         </select>
                            <?php echo form_error('location', '<span class="note error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>
                
            </p>
            <p>
                <input type="submit" class="submit small" name="submit" value="Submit"/>
            </p>
        </form>

   
</div> <!-- .block_content ends -->

    <div class="bendl"></div>
    <div class="bendr"></div>
     </div>
<?
//include_once 'semifooter.php';
//include_once 'footer.php';
?>
<style>
    #salesmanList, #clientList, #locationList
    {
        height: inherit;
        width:250px;
    }
</style>
<script type="text/javascript">
    $('select[name="client"]').change(function() { 
       var clientId = $("#clientList").val();
       var dataString = 'client_id='+clientId;
       var url = "<?php echo $baseurl.'clients/get_location/'; ?>"+clientId;
       $.ajax({
            type: "GET",
            url: url,
            data: dataString,
            cache: false,
            success: function(msg)
                {
                    if(msg){
                        $('#locationList')
                        .find('option')
                        .remove();
                        $('#locationList').append(msg);
                    }else{
                        $('#locationList')
                        .find('option')
                        .remove();
                        $('#locationList').append('<option value="">No Location available</option>');
                
                    }
            },
            error: function(msg){
                alert(msg);
                 $('#locationList')
                        .find('option')
                        .remove();
                    $('#locationList').append('<option value="">No Location available</option>');
                return false;
            }
        });
   });
   
</script>